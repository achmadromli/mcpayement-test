package com.mcpayment.test.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.mcpayment.test.model.Transaction;
import com.mcpayment.test.repository.TransactionRepository;

@RestController
@RequestMapping(value = "/api")
public class TransactionController {

    @Autowired
    private TransactionRepository transactionService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/transaction", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<Transaction> getTransactions() {
        return transactionService.findAll();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/transaction", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public Map<String, Object> saveTransaction(@RequestBody Transaction transaction) {
    	transactionService.save(transaction);

        Map<String, Object> m = new HashMap<>();
        m.put("Success", Boolean.TRUE);
        m.put("Info", "Data Tersimpan");

        return m;
    }
 
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/transaction", method = RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public Map<String, Object> updateTransaction(@RequestBody Transaction transaction) {
    	transactionService.save(transaction);

        Map<String, Object> m = new HashMap<>();
        m.put("Success", Boolean.TRUE);
        m.put("Info", "Data Berhasil di update");

        return m;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/transaction/{idTransaction}", method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Map<String, Object> getOneTransaction(@PathVariable("idTransaction") Long idTransaction) {
    	transactionService.findById(idTransaction);

        Map<String, Object> m = new HashMap<>();
        m.put("Success", Boolean.TRUE);
        m.put("Info", "Data Berhasil di temukan");

        return m;
    }
}