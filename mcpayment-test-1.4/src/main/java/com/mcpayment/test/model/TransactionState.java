package com.mcpayment.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TransactionState {
	@JsonProperty("SUCCESS")
	SUCCESS,
	@JsonProperty("CANCELED")
	CANCELED,
	
}
