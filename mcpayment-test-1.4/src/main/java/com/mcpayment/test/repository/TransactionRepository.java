package com.mcpayment.test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mcpayment.test.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

	
}
